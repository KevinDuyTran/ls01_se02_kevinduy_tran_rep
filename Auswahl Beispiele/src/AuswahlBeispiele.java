
public class AuswahlBeispiele {

	public static void main(String[] args) {
		
		int z1= 5;
		int z2 = 7;
		
		int erg = min(z2,z1);
		int erg2 = max(z2,z1);
		double erg3 = max2(2.5 , 7.2 , 3.6);
		
		System.out.println("Ergebnis: " + erg);
		System.out.println("Ergebnis: " + erg2);
		System.out.println("Ergebnis: " + erg3);

	}
	
	public static double max2(double zahl1, double zahl2, double zahl3) {
		double erg3;
		if(zahl1 > zahl2 && zahl1 > zahl3) {
			erg3 = zahl1;
		}else {
			if(zahl2 > zahl1 && zahl2 > zahl3) {
				erg3 = zahl2;
			}else {
				erg3 = zahl3;
			}
		}
		return erg3;
	}
	
	public static int max(int zahl1, int zahl2) {
		int erg2;
		if(zahl1>zahl2) {
			erg2 =zahl1;
		}else {
			erg2=zahl2;
		}
		return erg2;
		
	}
	
	public static int min(int zahl1, int zahl2) {
		int erg;
		if(zahl1<zahl2) {
			erg = zahl1;
		}else {
			erg = zahl2;
		}
		return erg;
	}

	
}
