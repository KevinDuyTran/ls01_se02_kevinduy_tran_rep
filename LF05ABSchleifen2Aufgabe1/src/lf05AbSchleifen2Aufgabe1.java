import java.util.Scanner;

public class lf05AbSchleifen2Aufgabe1 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		int eingabeZahl = 0;
		int summe = 0;
		int zaehler = 0;
		
		System.out.println("Geben Sie eine Zahl ein: ");
		eingabeZahl = myScanner.nextInt();
		
		while(zaehler <= eingabeZahl) {
			System.out.print(zaehler + " ");
			if (zaehler == eingabeZahl) {
				System.out.print(" = ");
			}
			else {
				System.out.print(" + ");
			}
			summe = summe + zaehler;
			
			zaehler++;			
		}
		System.out.println(summe);
		
		int zaehler2 = eingabeZahl;
		int summe2 = 0;
		while(zaehler2 >= 0) {
			System.out.print(zaehler2 + " ");
			if(zaehler2 > 0) {
				System.out.print(" + ");
			}
			else {
				System.out.print(" = ");
			}
			summe2 = summe2 + zaehler2;
			
			zaehler2--;
		}
		System.out.print(summe2);

	}

}
