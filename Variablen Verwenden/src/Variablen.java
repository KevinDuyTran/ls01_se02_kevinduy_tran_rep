﻿

/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	  long zaehlerProgrammDurchlaeufe;

    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  zaehlerProgrammDurchlaeufe = 25l;
	  System.out.println(zaehlerProgrammDurchlaeufe + " Durchläufe");

    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
	 char menuPointSelection;

    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	 menuPointSelection = 'C';
	 System.out.println("Your Menupoint-Selection: " + menuPointSelection);
	 
    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	 final long LIGHTSPEED;

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	 LIGHTSPEED = 299792458l; //Angabe in m/s
	 System.out.println("Die Lichtgeschwindigkeit ist: " + LIGHTSPEED + " m/s");
	 	
	 
    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	 long mitgliederAnzahlGesamt = 7l;

    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	 System.out.println("= = = Der Verein fasst aktuell: " + mitgliederAnzahlGesamt + " Mitglieder = = =");

    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	 String elektrischeElementarladung = "1,602⋅10−19As";
	 System.out.println("Die elektrische Elementarladung ist eine Konstante mit dem Wert: " + elektrischeElementarladung);

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	 boolean istZahlErfolgt;

    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	 istZahlErfolgt = true;
	 if (istZahlErfolgt = true) {
		 System.out.println(istZahlErfolgt + "! Die Zahl ist im Buchaltungsprogramm erfolgt!");
	 }else {
		 System.out.println(istZahlErfolgt + "! Die Zahl ist nicht im Buchhaltungsprogramm erfolgt!");
	 }
	 
  }//main
}// Variablen