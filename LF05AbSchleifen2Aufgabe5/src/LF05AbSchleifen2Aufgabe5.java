import java.util.Scanner;

public class LF05AbSchleifen2Aufgabe5 {

	public static void main(String[] args) {

		while (true) {
			// deklarierung aller notwendigen variablen
			double endKapital;
			double kapital = 0;
			double startKapital = 0;
			int vertragslaufzeit = 0;
			double zinssatz = 0;
			double zinsen = 0;
			int durchlaufCounter = 0; // ben�tigt um zu z�hlen, wie viele Jahre in der Rechnung vergangen sind
			Scanner myScanner = new Scanner(System.in);

			System.out.println("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =");
			System.out.println("= = = HERZLICH WILLKOMMEN BEI DER SPARKASSEN FINANZGRUPPE = = =");
			System.out.println("= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = \n");
			// Nutzereingaben
			// Eingabe Startkapital
			System.out.println("Wie hoch ist ihr Kapital was sie auf dieses Konto einzahlen wollen?");
			System.out.print("Ihre Eingabe (Bitte im Format 00,00): ");
			kapital = myScanner.nextDouble();
			startKapital = kapital;
			System.out.println("== Vielen Dank! Weiter zum n�chsten Schritt! === \n");

			// Eingabe Vertragslaufzeit
			System.out.println("Wie lange geht ihr Sparvertrag?");
			System.out.print("Ihre Eingabe in Jahren (Format: ganze Zahl): ");
			vertragslaufzeit = myScanner.nextInt();
			System.out.println("== Vielen Dank! Weiter zum n�chsten Schritt! === \n");

			// Eingabe Zinssatz
			System.out.println("Wie hoch ist der im Vertrag festgelegte Zinssatz?");
			System.out.print("Ihre Eingabe in Prozent (Format 00,00): ");
			zinssatz = myScanner.nextDouble();
			System.out.println("== Vielen Dank! Weiter zum n�chsten Schritt! === \n");

			// Berechnung
			while (durchlaufCounter < vertragslaufzeit) {
				zinsen = (kapital / 100) * zinssatz; // wert : 100% * xx% = xx% vom wert wo wir die Zinsen drauf
														// addieren wollen
				kapital = kapital + zinsen;
				durchlaufCounter++;

			}
			endKapital = kapital;

			// finale Ausgabe
			System.out.println("Ihr Startkapital lautet: " + startKapital + "�");
			System.out.println("Ihr Endkapital wird lauten: " + endKapital + "�");
			System.out.println("\n\n\n\n\n"); // F�r einen sauberen Neuanfang
		}

	}

}
