import java.util.Scanner;

public class lf05AbSchleifen2Aufgabe3 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int eingabeZahl = 0;
		int ersterSpeicher;
		int zweiterSpeicher;
		int quersumme = 0;

		System.out.println("Geben Sie eine Zahl ein: ");
		eingabeZahl = myScanner.nextInt();

		ersterSpeicher = eingabeZahl % 10;
		zweiterSpeicher = eingabeZahl / 10;
		quersumme = ersterSpeicher + zweiterSpeicher;
		System.out.println("Die Quersummer von " + eingabeZahl + " lautet: " + quersumme);

	}

}
