﻿/*
1. 	Der Vorteil, die Strings durch Arrays zu ersetzen, liegt in der einfachen Skalierbarkeit des Auswahlmenüs sowie des Preises. 
	Ebenso sind Veränderungen/Anpassungen effizienter durchzuführen, da man weniger Werte ändern muss.

3. 	Vorteile: Das Automatenmenü lässt sich definitiv besser an sich ändernde Produktpaletten anpassen.
	Der Code ist übersichtlicher geworden.
	Nachteile: Ein Nachteil wäre, das initiale Umändern des Codes nimmt definitiv mehr Zeit in Anspruch und fordert definitiv sauberes/genaues Arbeiten um Fehler zu vermeiden.
*/

import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		// Deklarierungen & Initialisierungen
		int wartungsZeitraum = 0;
		String[] fahrkartenBezeichnung = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",
				"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };
		double[] fahrkartenPreis = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };

		while (wartungsZeitraum < 999) {
			rueckgeldAusgeben(fahrkartenBezeichnung, fahrkartenPreis);
			wartungsZeitraum += 1;
		}
		System.out.println(
				"Der Automat hat das Ende seines Wartungszeitraumes erreicht. Bitte kontaktieren Sie den Kundenservice!");
		// Wir hätten hier auch while(true) verwenden können, aber so ein
		// Wartungszeitraum fügt noch ein bisschen extra Realismus-Pepp hinzu :D

	}

	// Bestellaufnahme und Rückgabe zuZahlen
	public static double fahrkartenbestellungErfassen(String[] fahrkartenBezeichnung, double[] fahrkartenPreis) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		for (int i = 0; i < fahrkartenPreis.length; i++) {
			System.out.println(fahrkartenBezeichnung[i] + fahrkartenPreis[i] + " (" + i + ")");
		}

		double preisTicket = 0;
		boolean standardAusgabe = false; // sorgt dafür, dass bei ungültiger Eingabe die Abfrage wiederholt wird

		while (standardAusgabe == false) {
			System.out.println("Ihre Wahl?: ");
			int fahrkartenArt = tastatur.nextInt();
			// If-Abfrage für Ticketauswahl zuständig
			if (fahrkartenArt < fahrkartenPreis.length) {
				preisTicket = (fahrkartenPreis[fahrkartenArt] * 100); // KONVERTIERUNG TICKETPREIS IN CENT
				standardAusgabe = true;
			} else {
				System.out.println("Ungültige Eingabe! Bitte Wählen Sie ein gültiges Ticket!");
				standardAusgabe = false;
			}

		}

		System.out.println("Wie viele Tickets wollen Sie kaufen?: ");
		int anzahlTicket = tastatur.nextInt();
		double zuZahlen = anzahlTicket * preisTicket;
		return zuZahlen;
	}

	public static double fahrkartenBezahlen(String[] fahrkartenBezeichnung, double[] fahrkartenPreis) {
		double zuZahlen = fahrkartenbestellungErfassen(fahrkartenBezeichnung, fahrkartenPreis);
		// Münzeinwurf
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("%s %.2f %s %n", "Noch zu zahlen: ", (zuZahlen - eingezahlterGesamtbetrag) / 100, "Euro"); // KONVERTIERUNG
																															// D.
																															// AUSGABE
																															// IN
																															// EURO
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = (tastatur.nextDouble() * 100); // KONVERTIEREN EINGEWORFENE MÜNZEN IN CENT
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		// Verrechnung zuZahlen & eingezahlter Gesamtbetrag
		double rueckgabeBetrag = (eingezahlterGesamtbetrag - zuZahlen) / 100; // KONVERTIERUNG rueckgabeBetrag IN EURO
		return rueckgabeBetrag;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe Benachrichtigung
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 50; i++) {
			System.out.print("=");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(String[] fahrkartenBezeichnung, double[] fahrkartenPreis) {
		double rueckgabeBetrag = fahrkartenBezahlen(fahrkartenBezeichnung, fahrkartenPreis); // Aufruf der
																								// fahrkartenBezahlen
																								// Methode da Wert
																								// rueckgabeBetrag
																								// benötigt wird
		fahrkartenAusgeben();
		if (rueckgabeBetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", (rueckgabeBetrag));
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rueckgabeBetrag >= 2.00) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rueckgabeBetrag -= 2.00;
			}
			while (rueckgabeBetrag >= 1.00) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rueckgabeBetrag -= 1.00;
			}
			while (rueckgabeBetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rueckgabeBetrag -= 0.50;
			}
			while (rueckgabeBetrag >= 0.20) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rueckgabeBetrag -= 0.20;
			}
			while (rueckgabeBetrag >= 0.10) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rueckgabeBetrag -= 0.10;
			}
			while (rueckgabeBetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rueckgabeBetrag -= 0.05;
			}
		}
	}

}