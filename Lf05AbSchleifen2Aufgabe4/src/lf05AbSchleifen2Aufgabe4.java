import java.util.Scanner;

public class lf05AbSchleifen2Aufgabe4 {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Geben Sie den Startwert in Celsius ein!");
		double startwert = myScanner.nextDouble();
		System.out.println("Geben Sie den Endwert in Celsius an!");
		double endwert = myScanner.nextDouble();
		System.out.println("Bitte die Schrittweite in Clesius angeben!");
		double schrittweite = myScanner.nextDouble();
		double cwert = startwert;
		double fwert = 0;

		while (cwert <= endwert) {
			fwert = cwert * 1.8 + 32;
			System.out.printf("%.2f�C%10.2f�F\n", cwert, fwert);
			cwert = cwert + schrittweite;
		}

	}

}
