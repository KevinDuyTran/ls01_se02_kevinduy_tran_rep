import java.util.Scanner;

public class lf05AbSchleifen2Aufgabe2 {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		int eingabeZahl = 0;
		int zaehler = 1;
		long produkt = 1;
		boolean korrekteEingabe = false;

		while (korrekteEingabe == false) {
			System.out.println("Bitte geben Sie eine Zahl ein:");
			eingabeZahl = myScanner.nextInt();

			if (eingabeZahl <= 20) {
				while (zaehler <= eingabeZahl) {
					System.out.print(zaehler + " ");
					if (zaehler == eingabeZahl) {
						System.out.print(" = ");
					} else {
						System.out.print(" * ");
					}
					produkt = produkt * zaehler;

					zaehler++;
				}
				System.out.println(produkt);
				korrekteEingabe = true;

			} else {
				System.out.println("Ihre Eingabe ist fehlerhaft: Zu gro�e Zahl!");
			}

		}

	}

}
