import java.util.Scanner;			//Import des Scanners aus class

public class Methodenbeispiele {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);			//deklarieren einer neuen Scanner-Variable
		
		String vname = leseString("Geben Sie Ihren Vornamen ein:");
		String nname = leseString("Geben Sie Ihren Nachnamen ein:");
		sayHello(vname,nname);
		
		
		Scanner altScanner = new Scanner(System.in);
		
		int alter = leseInt("Geben Sie bitte Ihr Alter an:");
		System.out.println("Alter: " + alter);
		

	}
	
	
	public static int leseInt(String alter) {
		System.out.println(alter);
		Scanner altScanner = new Scanner(System.in);
		
		int alt = altScanner.nextInt();
		
		return alt;
		
	}
	
	
	public static void sayHello(String vname, String nname) {			
		System.out.println("Hello " + vname + "" + nname);
	}
	
	public static String leseString(String text) { 			//text ist String oben aus Methodenaufruf
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		
		String str = myScanner.next();
		
		return str;
		
	}
	
}
