
public class ArrayBeispiele {

	public static void main(String[] args) {

		int[] zahlenliste = { 1, 2, 3, 4 };
		int[] zahlenliste2 = new int[4];
		zahlenliste2[0] = 1;
		zahlenliste2[1] = 2;
		zahlenliste2[2] = 3;
		zahlenliste2[3] = 4;

		int[] zahlenliste3 = new int[100];

		for (int i = 0; i < zahlenliste3.length; i++) {
			zahlenliste3[i] = i + 1;
		}

		System.out.println(zahlenliste[0]);
		System.out.println(zahlenliste2[1]);

		for (int i = 0; i < zahlenliste3.length; i++) {

			System.out.print(zahlenliste3[i] + " ");

		}

	}

}
