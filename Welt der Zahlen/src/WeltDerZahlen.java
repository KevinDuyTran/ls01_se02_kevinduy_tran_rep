/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstraße
    String anzahlSterne = "ca. 100 bis 400 Milliarden";
    
    // Wie viele Einwohner hat Berlin?
    long bewohnerBerlin = 3669098l;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
     long alterTage = 7819l;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
     long gewichtKilogramm = 190000l; 
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
     long flaecheGroessteLand = 17098242l;
    
    // Wie groß ist das kleinste Land der Erde?
    
     float flaecheKleinsteLand = 0.44f;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten + " Planeten");
    System.out.println("Anzahl der Sterne: " + anzahlSterne + " Sterne");
    System.out.println("Bewohner Berlins (Stand M�rz 2020): " + bewohnerBerlin + " Bewohner");
    System.out.println("Atler von Kevin Duy Tran in Tagen;: " + alterTage + " Tage");
    System.out.println("Gewicht des schwersten Tieres der Welt: " + gewichtKilogramm + " kg");
    System.out.println("Gr��te Stadt der Welt (in km�): " + flaecheGroessteLand + " km�");
    System.out.println("Kleinste Stadt der Welt (in km�): " + flaecheKleinsteLand + " km�");
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

